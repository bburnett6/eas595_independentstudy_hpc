#!/bin/bash
nvcc fractalGen.cu -o fractalGenCU -g -G -Xcompiler "-fopenmp" -I/home/bburnett/.local/include -L/home/bburnett/.local/lib64 -lopencv_core -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs
g++ fractalGen.cpp -o fractalGenMP -fopenmp  -I/home/bburnett/.local/include -L/home/bburnett/.local/lib64 -lopencv_core -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs
