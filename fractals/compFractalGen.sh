#!/bin/bash
nvcc fractalGen.cu -o fractalGenCU -Xcompiler "-fopenmp" -lopencv_core -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs
g++ fractalGen.cpp -o fractalGenMP -fopenmp -lopencv_core -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs
