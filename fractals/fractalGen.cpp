#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <omp.h>
#include <iostream>
#include <iomanip>

#define MAX_IT 256

struct uchar4
{
	unsigned char x;
	unsigned char y;
	unsigned char z;
	unsigned char w;
};

struct int2
{
	int x;
	int y;
};

struct float3
{
	float x;
	float y;
	float z;
};

//GPU Support

uchar4 devjulia(float real, float imag, float a)
{
	int count = 0;
	float zReal = real;
	float zImag = imag;
	float nextRe = 0.0;
	float cReal = 0.7885 * cos(a);
	float cImag = 0.7885 * sin(a);
	float sColor = 0.0;
	float n = 2.0;

	while (zReal * zReal + zImag * zImag <= 4.0 && count <= MAX_IT)
	{
		nextRe = powf(zReal * zReal + zImag * zImag, n / 2.0) * cos(n * atan2(zImag, zReal)) + cReal;
		zImag = powf(zReal * zReal + zImag * zImag, n / 2.0) * sin(n * atan2(zImag, zReal)) + cImag;
		zReal = nextRe;
		sColor += exp(- (zReal * zReal + zImag * zImag));
		count++;
	}

	uchar4 p;
	p.x = 0;
	p.y = sin(sColor * 2 * M_PI) * 255;
	p.z = 0;
	return p;

}

void pixelKernel(uchar4 *pixel, float3 g, float a, int2 screen, int x, int y)
{
	float real = (x - screen.x / 2.0) * g.x + g.y;
	float imag = (y - screen.y / 2.0) * g.x + g.z;
	*pixel = devjulia(real, imag, a);
}

int main(int argc, char **argv)
{
	if (argc < 4 || argc > 8)
	{
		printf("Usage: ./simpleFractal num_threads screen_width screen_height print_header(1,0)\n");
		exit(1);
	}

	int numt = atoi(argv[1]);
	int2 screen;
	screen.x = atoi(argv[2]);
	screen.y = atoi(argv[3]);
	bool print_header = atoi(argv[4]) == 1 ? true : false;

	//globale fractal constants zoom, x offset, y offset
	float3 globe;
	globe.x = 0.004 / (screen.x / 500); //zoom
	globe.y = 0.0; //xoff
	globe.z = 0.0; //yoff

	//printf("%d, %d\n", screen.x, screen.y);
	uchar4 *pixels = (uchar4*)calloc(screen.x * screen.y, sizeof(uchar4));
	for (int x = 0; x < screen.x; x++)
	{
		for (int y = 0; y < screen.y; y++)
		{
			if (x == y)
				pixels[y * screen.y + x].x = 255;
		}
	}

	cv::Mat frac(screen.x, screen.y, CV_8UC3, cv::Scalar(0, 0, 0));
	cv::VideoWriter out("testRotate.avi", cv::VideoWriter::fourcc('M','J', 'P', 'G'), 20.0, cv::Size(screen.x, screen.y));
	if(!out.isOpened()) 
	{
		printf("Error! Unable to open video file for output.");
		std::exit(-1);
	}

	float a = 0.0;
	float start = omp_get_wtime();
	int nframes = 0;
	#pragma omp parallel  num_threads(numt) private(a) shared(pixels, globe, screen)
	while (a <= 2 * M_PI)
	{
		#pragma omp for
		for (int x = 0; x < screen.x; x++)
		{
			for (int y = 0; y < screen.y; y++)
			{
				//uchar4 *myPix = &pixels[y * screen.y + x];
				pixelKernel(&pixels[y * screen.y + x], globe, a, screen, x, y);
				cv::Vec3b color = frac.at<cv::Vec3b>(cv::Point(x, y));
				color[0] = pixels[y * screen.y + x].x;
				color[1] = pixels[y * screen.y + x].y;
				color[2] = pixels[y * screen.y + x].z;
				frac.at<cv::Vec3b>(cv::Point(x,y)) = color;
			}
		}
		#pragma omp single
		{
			nframes++;
			out.write(frac);
		}

		a += 0.01;
	}
	float time = omp_get_wtime() - start;
	if (print_header)
		std::cout << std::setw(7) << "screenx" << " | " << std::setw(7) << "screeny" << " | " << std::setw(10) << "NFrames" << " | " << 
			std::setw(10) << "FPS" << " | " << std::setw(10) << "TotTime" << std::endl;
	std::cout << std::setprecision(3) << std::setw(7) << screen.x << " | " << std::setw(7) << screen.y << " | " <<
		std::setw(10) << nframes << " | " << std::setw(10) << nframes / time << " | " << std::setw(10) << time << std::endl;

	return 0;
}
