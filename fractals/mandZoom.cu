#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <complex>
#include <vector>

//SDL functions
#define SCREEN_WIDTH 500
#define SCREEN_HEIGHT 500
#define MAX_IT 512
SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
bool init();
void close();

//GPU Support
__device__ float dmin(float a, float b)
{
	return a < b ? a : b;
}

__device__ float dmax(float a, float b)
{
	return a > b ? a : b;
}

__device__ int devjulia(float real, float imag)
{
	int count = 0;
	float zReal = real;
	float zImag = imag;
	float nextRe = 0.0;
	float cReal = -0.8;
	float cImag = 0.156;
	float n = 2.0;

	while (zReal * zReal + zImag * zImag <= 4.0 && count <= MAX_IT)
	{
		nextRe = pow(zReal * zReal + zImag * zImag, n / 2.0) * cos(n * atan2(zImag, zReal)) + cReal;
		zImag = pow(zReal * zReal + zImag * zImag, n / 2.0) * sin(n * atan2(zImag, zReal)) + cImag;
		zReal = nextRe;
		count++;
	}

	if (count == MAX_IT)
		return -1;
	else
		return count;
}

__device__ uchar4 devcoloring2(int c)
{
	double q = (double) c / (double) MAX_IT;
	double col = dmax(0.0, dmin(q, 1.0)) * 255.0;
	uchar4 ret;
	if (q > 0.5)
	{
		ret.x = (int) col;
		ret.y = 255;
		ret.z = (int) col;
	}
	else
	{
		ret.x = 0;
		ret.y = (int) col;
		ret.z = 0;
	}

	return ret;
}

__global__ void pixelKernel(uchar4 *pixels, float3 g)
{
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int i = row * SCREEN_WIDTH + col;

	if ((col > SCREEN_WIDTH) || (row > SCREEN_HEIGHT))
		return;

	float real = (col - SCREEN_WIDTH / 2.0) * g.x + g.y;
	float imag = (row - SCREEN_HEIGHT / 2.0) * g.x + g.z;
	int c = devjulia(real, imag);
	pixels[i] = devcoloring2(c);
	if (col == row)
		pixels[i].y = 255;
}

int main()
{
	float3 globe;
	globe.x = 0.004; //zoom
	globe.y = 0.0; //xoff
	globe.z = 0.0; //yoff
	uchar4 *pixels = (uchar4*)calloc(SCREEN_WIDTH * SCREEN_HEIGHT, sizeof(uchar4));
	for (int x = 0; x < SCREEN_WIDTH; x++)
	{
		for (int y = 0; y < SCREEN_HEIGHT; y++)
		{
			if (x == y)
				pixels[y * SCREEN_HEIGHT + x].x = 255;
		}
	}

	uchar4 *devPixels;
	if (cudaMalloc(&devPixels, SCREEN_WIDTH * SCREEN_HEIGHT * sizeof(uchar4)))
		printf("cuda malloc error\n");
	if(cudaMemcpy(devPixels, pixels, SCREEN_WIDTH * SCREEN_HEIGHT * sizeof(uchar4), cudaMemcpyHostToDevice))
		printf("1st cudamemcpy error\n");
	dim3 blockSize(32, 32);
	int gridx = (SCREEN_WIDTH + blockSize.x - 1) / blockSize.x;
	int gridy = (SCREEN_HEIGHT + blockSize.y - 1) / blockSize.y;
	dim3 gridSize(gridx, gridy);

	bool stateChange = true;
	bool quit = false;
	SDL_Event e;
	if (!init())
	{
		quit = true;
		printf("SDL initialization failed...\n");
	}

	while(!quit)
	{
		while( SDL_PollEvent( &e ) != 0 )
		{
			if( e.type == SDL_QUIT )
			{
				quit = true;
				printf("quitting\n");
			}
			if (e.type == SDL_KEYUP && e.key.repeat == 0)
			{
				printf("Got your input\n");
				stateChange = true;
				switch (e.key.keysym.sym) 
				{
					case SDLK_EQUALS:
						globe.x *= 0.9;
						break;
					case SDLK_MINUS:
						globe.x /= 0.9;
						break;
					case SDLK_LEFT:
						globe.y -= 40 * globe.x;
						break;
					case SDLK_RIGHT:
						globe.y += 40 * globe.x;
						break;
					case SDLK_UP:
						globe.z -= 40 * globe.x;
						break;
					case SDLK_DOWN:
						globe.z += 40 * globe.x;
						break;
				}
			}
			if (e.type == SDL_MOUSEBUTTONDOWN)
			{
				printf("Got your input\n");
				int x, y;
				SDL_GetMouseState(&x, &y);
				globe.y += (x - SCREEN_WIDTH / 2.0) * globe.x;
				globe.z += (y - SCREEN_HEIGHT / 2.0) * globe.x;	
			}
		}

		if (stateChange)
		{
			printf("starting to draw...\n");
			SDL_SetRenderDrawColor( gRenderer, 255, 255, 255, 255 );
			SDL_RenderClear( gRenderer );
			pixelKernel<<<gridSize, blockSize>>>(devPixels, globe);
			int cudaErr = 0;
			cudaErr = cudaMemcpy(pixels, devPixels, SCREEN_WIDTH * SCREEN_HEIGHT * sizeof(uchar4), cudaMemcpyDeviceToHost);
			if (cudaErr)
				printf("Draw time cudaMemcpy error: %d\n", cudaErr);

			for (int x = 0; x < SCREEN_WIDTH; x++)
			{
				for (int y = 0; y < SCREEN_HEIGHT; y++)
				{
					int i = y * SCREEN_WIDTH + x;
					SDL_SetRenderDrawColor(gRenderer, pixels[i].x, pixels[i].y, pixels[i].z, 0);
					SDL_RenderDrawPoint(gRenderer, x, y);
				}
			}
			SDL_RenderPresent(gRenderer);
			printf("Now waiting for input\n");
			stateChange = false;
		}
	}

	cudaFree(devPixels);
	close();
	return 0;
}

/*
	======================
	SDL functions
	======================
*/
bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "Mandelbrot Zoomer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

void close()
{
	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}
