#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <omp.h>
#include <mpi.h>
#include <iostream>
#include <iomanip>
#include <sstream>

#define MAX_IT 256

//GPU Support

__device__ uchar4 devjulia(float real, float imag, float a)
{
	int count = 0;
	float zReal = real;
	float zImag = imag;
	float nextRe = 0.0;
	float cReal = 0.7885 * cos(a);
	float cImag = 0.7885 * sin(a);
	float sColor = 0.0;
	float n = 2.0;

	while (zReal * zReal + zImag * zImag <= 4.0 && count <= MAX_IT)
	{
		nextRe = powf(zReal * zReal + zImag * zImag, n / 2.0) * cos(n * atan2(zImag, zReal)) + cReal;
		zImag = powf(zReal * zReal + zImag * zImag, n / 2.0) * sin(n * atan2(zImag, zReal)) + cImag;
		zReal = nextRe;
		sColor += exp(- (zReal * zReal + zImag * zImag));
		count++;
	}

	uchar4 p;
	p.x = 0;
	p.y = sin(sColor * 2 * M_PI) * 255;
	p.z = 0;
	return p;

}

__global__ void pixelKernel(uchar4 *pixels, float3 g, float a, int2 screen)
{
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int i = row * screen.x + col;

	if ((col >= screen.x) || (row >= screen.y))
		return;

	float real = (col - screen.x / 2.0) * g.x + g.y;
	float imag = (row - screen.y / 2.0) * g.x + g.z;
	pixels[i] = devjulia(real, imag, a);
}

int main(int argc, char **argv)
{
	if (argc < 5 || argc > 9)
	{
		printf("Usage: ./simpleFractal num_threads screen_width screen_height block_size(1,2,3,4,5) print_header(1,0)\n");
		exit(1);
	}

	int size, rank;
	int provided, requested = MPI_THREAD_MULTIPLE;
	//MPI_Init(&argc, &argv);
	MPI_Init_thread(&argc, &argv, requested, &provided);
	if (provided < requested)
	{
		printf("MPI Threading level not available...\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int numt = atoi(argv[1]);
	int2 screen;
	screen.x = atoi(argv[2]);
	screen.y = atoi(argv[3]);
	int block_size = (int) pow(2, atoi(argv[4]));
	bool print_header = atoi(argv[5]) == 1 ? true : false;

	//globale fractal constants zoom, x offset, y offset
	float3 globe;
	globe.x = 0.004 / (screen.x / 500); //zoom
	globe.y = 0.0; //xoff
	globe.z = 0.0; //yoff

	//printf("%d, %d\n", screen.x, screen.y);
	uchar4 *pixels = (uchar4*)calloc(screen.x * screen.y, sizeof(uchar4));
	for (int x = 0; x < screen.x; x++)
	{
		for (int y = 0; y < screen.y; y++)
		{
			if (x == y)
				pixels[y * screen.y + x].x = 255;
		}
	}

	std::stringstream outfname;
	outfname << "testRotate" << rank << ".avi";
	cv::Mat frac(screen.x, screen.y, CV_8UC3, cv::Scalar(0, 0, 0));
	cv::VideoWriter out(outfname.str(), cv::VideoWriter::fourcc('M','J', 'P', 'G'), 20.0, cv::Size(screen.x, screen.y));
	if(!out.isOpened()) 
	{
		printf("Error! Unable to open video file for output.");
		std::exit(-1);
	}

	uchar4 *devPixels;
	cudaError_t cudaErr;
	cudaErr = cudaMalloc((void **)&devPixels, screen.x * screen.y * sizeof(uchar4));
	if (cudaErr)
		printf("cuda malloc error %d on line %d\n", cudaErr, __LINE__);
	cudaErr = cudaMemcpy(devPixels, pixels, screen.x * screen.y * sizeof(uchar4), cudaMemcpyHostToDevice);
	if(cudaErr)
		printf("cudaMemcpy error %d on line %d\n", cudaErr, __LINE__);

	dim3 blockSize(block_size, block_size);
	int gridx = (screen.x + blockSize.x - 1) / blockSize.x;
	int gridy = (screen.y + blockSize.y - 1) / blockSize.y;
	dim3 gridSize(gridx, gridy);

	//printf("starting gpu...\n");

	float a_max = (rank + 1) * 2 * M_PI / size;
	float a = rank * 2 * M_PI / size;
	float start = omp_get_wtime();
	int nframes = 0;
	//printf("rank: %d, a: %f, a_max: %f\n", rank, a, a_max);
	#pragma omp parallel  num_threads(numt) firstprivate(a) shared(pixels, globe, screen)
	while (a <= a_max)
	{
		#pragma omp single
		{
			pixelKernel<<<gridSize, blockSize>>>(devPixels, globe, a, screen);
			cudaDeviceSynchronize();
			
			cudaErr = cudaMemcpy(pixels, devPixels, screen.x * screen.y * sizeof(uchar4), cudaMemcpyDeviceToHost);
			if (cudaErr)
				printf("cudaMemcpy error %d on line %d\n", cudaErr, __LINE__);
		}

		#pragma omp barrier
		#pragma omp for
		for (int x = 0; x < screen.x; x++)
		{
			for (int y = 0; y < screen.y; y++)
			{
				//uchar4 *myPix = &pixels[y * screen.y + x];
				cv::Vec3b color = frac.at<cv::Vec3b>(cv::Point(x, y));
				color[0] = pixels[y * screen.y + x].x;
				color[1] = pixels[y * screen.y + x].y;
				color[2] = pixels[y * screen.y + x].z;
				frac.at<cv::Vec3b>(cv::Point(x,y)) = color;
			}
		}
		#pragma omp single
		{
			out.write(frac);
			//cv::imwrite("./frac.png", frac);
			nframes++;
		}
		a += 0.01;
	}

	float time = omp_get_wtime() - start;
	if (print_header && rank == 0)
		std::cout << std::setw(7) << "screenx" << " | " << std::setw(7) << "screeny" << " | " << std::setw(10) << "NFrames" << " | " << 
			std::setw(10) << "FPS" << " | " << std::setw(10) << "TotTime" << std::endl;
	std::cout << std::setprecision(3) << std::setw(7) << screen.x << " | " << std::setw(7) << screen.y << " | " <<
		std::setw(10) << nframes << " | " << std::setw(10) << nframes / time << " | " << std::setw(10) << time << std::endl;
	//printf("Nframes,FPS,TotTime: %d,%f,%f\n", nframes, nframes / time, time);
	cudaFree(devPixels);
	MPI_Finalize();
	return 0;
}
