#!/bin/bash
#
#SBATCH --job-name=FractalGen
#SBATCH --nodes=2 --ntasks-per-node=1
#SBATCH --output=out-%j.out
#SBATCH --gres=gpu:V100:1
#SBATCH --mem=4096


module purge
module load cuda
module load gcc
module load openmpi

sh mpicomp.sh

mpirun ./fractalGen_mpi 8 1000 1000 4 1

for filename in *.avi; do
	echo "file '$filename'" >> vidlist.txt
done

ffmpeg -safe 0 -f concat -i vidlist.txt -c copy final.avi

rm vidlist.txt
