#!/bin/bash

#module load gcc
#module list

#sh mpicomp.sh

#mpirun -n 2 ./fractalGen_mpi 8 1000 1000 4 1

for filename in *.avi; do
	echo "file '$filename'" >> vidlist.txt
done

ffmpeg -safe 0 -f concat -i vidlist.txt -c copy final.avi
