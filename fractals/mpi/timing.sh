#!/bin/bash
#
#SBATCH --job-name=FractalTiming
#SBATCH --nodes=3 --ntasks-per-node=1
#SBATCH --output=out-%j.out
#SBATCH --gres=gpu:V100:1
#SBATCH --mem=4096


module purge
module load cuda
module load gcc
module load openmpi

sh mpicomp.sh

echo "3 nodes" >> timingOut.txt
mpirun ./fractalGen_mpi 8 500 500 4 1 >> timingOut.txt
for r in 1000 1500 2000 2500 3000 3500 4000
do
        mpirun ./fractalGen_mpi 8 $r $r 4 0 >> timingOut.txt
done

