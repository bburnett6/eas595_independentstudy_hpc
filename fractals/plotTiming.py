import matplotlib.pyplot as plt 

runs = []
name = "" 
run = {}
size = []
time = []
with open("timingOut.txt", 'r') as f:
	f.readline()
	for line in f.readlines():
		sp = line.split(' | ')
		try:
			size.append(int(sp[0]))
			time.append(float(sp[-1]))
		except:
			if (sp[0] == "screenx"):
				continue
			else:
				if name:
					run["size"] = size
					run["time"] = time
					run["name"] = name.strip()
					runs.append(run)
					run = {}
					time = []
					size = []
				name = sp[0]

fig = plt.figure(figsize=(10,8), dpi=80)
for run in runs:
	plt.plot(run["size"], run["time"], label=run["name"])

plt.title("Fractal Generation Run Time With Variable GPU Grid Size and OpenMP")
plt.xlabel("Size of x,y Dimension")
plt.ylabel("Program Run Time")
plt.ylim([0,600])
plt.legend()
plt.savefig("results.pdf")

fig = plt.figure(figsize=(10,8), dpi=80)
for run in runs:
	if ("OpenMP" not in run["name"]):
		plt.plot(run["size"], run["time"], label=run["name"])

plt.title("Fractal Generation Run Time With Variable GPU Grid Size Only")
plt.xlabel("Size of x,y Dimension")
plt.ylabel("Program Run Time")
plt.legend()
plt.savefig("gpuOnly.pdf")