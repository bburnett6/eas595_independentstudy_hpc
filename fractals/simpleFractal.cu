#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_IT 512

//GPU Support
__device__ float dmin(float a, float b)
{
	return a < b ? a : b;
}

__device__ float dmax(float a, float b)
{
	return a > b ? a : b;
}

__device__ int devjulia(float real, float imag)
{
	int count = 0;
	float zReal = real;
	float zImag = imag;
	float nextRe = 0.0;
	float cReal = -0.8;
	float cImag = 0.156;
	float n = 2.0;

	while (zReal * zReal + zImag * zImag <= 4.0 && count <= MAX_IT)
	{
		nextRe = powf(zReal * zReal + zImag * zImag, n / 2.0) * cos(n * atan2(zImag, zReal)) + cReal;
		zImag = powf(zReal * zReal + zImag * zImag, n / 2.0) * sin(n * atan2(zImag, zReal)) + cImag;
		zReal = nextRe;
		count++;
	}

	if (count == MAX_IT)
		return -1;
	else
		return count;
}

__device__ uchar4 devcoloring2(int c)
{
	double q = (double) c / (double) MAX_IT;
	double col = dmax(0.0, dmin(q, 1.0)) * 255.0;
	uchar4 ret;
	if (q > 0.5)
	{
		ret.x = (int) col;
		ret.y = 255;
		ret.z = (int) col;
	}
	else
	{
		ret.x = 0;
		ret.y = (int) col;
		ret.z = 0;
	}

	return ret;
}

__global__ void pixelKernel(uchar4 *pixels, float3 g, int2 screen)
{
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int i = row * screen.x + col;

	if ((col >= screen.x) || (row >= screen.y))
		return;

	float real = (col - screen.x / 2.0) * g.x + g.y;
	float imag = (row - screen.y / 2.0) * g.x + g.z;
	int c = devjulia(real, imag);
	pixels[i] = devcoloring2(c);
	if (col == row)
		pixels[i].y = 255;
}

int main(int argc, char **argv)
{
	if (argc < 2 || argc > 4)
	{
		printf("Usage: ./simpleFractal screen_width screen_height\n");
		exit(1);
	}
	float3 globe;
	globe.x = 0.004; //zoom
	globe.y = 0.0; //xoff
	globe.z = 0.0; //yoff

	int2 screen;
	screen.x = atoi(argv[1]);
	screen.y = atoi(argv[2]);

	printf("%d, %d\n", screen.x, screen.y);
	uchar4 *pixels = (uchar4*)calloc(screen.x * screen.y, sizeof(uchar4));
	for (int x = 0; x < screen.x; x++)
	{
		for (int y = 0; y < screen.y; y++)
		{
			if (x == y)
				pixels[y * screen.y + x].x = 255;
		}
	}

	uchar4 *devPixels;
	if (cudaMalloc((void **)&devPixels, screen.x * screen.y * sizeof(uchar4)))
		printf("cuda malloc error\n");
	if(cudaMemcpy(devPixels, pixels, screen.x * screen.y * sizeof(uchar4), cudaMemcpyHostToDevice))
		printf("1st cudamemcpy error\n");
	dim3 blockSize(16, 16);
	int gridx = (screen.x + blockSize.x - 1) / blockSize.x;
	int gridy = (screen.y + blockSize.y - 1) / blockSize.y;
	dim3 gridSize(gridx, gridy);

	printf("starting gpu...\n");
	pixelKernel<<<gridSize, blockSize>>>(devPixels, globe, screen);
	cudaDeviceSynchronize();
	int cudaErr = 0;
	cudaErr = cudaMemcpy(pixels, devPixels, screen.x * screen.y * sizeof(uchar4), cudaMemcpyDeviceToHost);
	if (cudaErr)
		printf("cudaMemcpy error: %d on line %d\n", cudaErr, __LINE__);


	cudaFree(devPixels);

	return 0;
}
