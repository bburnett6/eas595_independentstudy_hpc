#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>

const int SCREEN_WIDTH = 1000;
const int SCREEN_HEIGHT = 600;

class Pixel {
public:
	Uint8 r;
	Uint8 g;
	Uint8 b;

	Pixel()
	{
		r = 0;
		g = 0;
		b = 0;
	}

	Pixel(Uint8 ir, Uint8 ig, Uint8 ib)
	{
		r = ir;
		g = ig;
		b = ib;
	}

};

int main()
{
	Pixel pixels[SCREEN_WIDTH * SCREEN_HEIGHT];
	for (int x = 0; x < SCREEN_WIDTH; x++)
	{
		for (int y = 0; y < SCREEN_HEIGHT; y++)
		{
			pixels[x * SCREEN_HEIGHT + y] = Pixel();
			printf("%d, %d\n", x, y);
		}
	}
	printf("hello\n");

	return 0;
}