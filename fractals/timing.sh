#!/bin/bash

sh compFractalGen.sh

date > timingOut.txt

#warm up the GPU
echo "warm up data ignore..."
./fractalGenCU 1 500 500 5 1
./fractalGenCU 1 500 500 5 1

#begin Tests
echo "Begining Tests:"
echo "GPU block size: 4x4"
echo "GPU block size: 4x4" >> timingOut.txt
./fractalGenCU 8 500 500 2 1 >> timingOut.txt
for r in 1000 1500 2000 2500 3000 3500 4000
do
	./fractalGenCU 8 $r $r 2 0 >> timingOut.txt
done

echo "GPU block size: 8x8"
echo "GPU block size: 8x8" >> timingOut.txt
./fractalGenCU 8 500 500 3 1 >> timingOut.txt
for r in 1000 1500 2000 2500 3000 3500 4000
do
	./fractalGenCU 8 $r $r 3 0 >> timingOut.txt
done

echo "GPU block size: 16x16"
echo "GPU block size: 16x16" >> timingOut.txt
./fractalGenCU 8 500 500 4 1 >> timingOut.txt
for r in 1000 1500 2000 2500 3000 3500 4000
do
	./fractalGenCU 8 $r $r 4 0 >> timingOut.txt
done

echo "GPU block size: 32x32"
echo "GPU block size: 32x32" >> timingOut.txt
./fractalGenCU 8 500 500 5 1 >> timingOut.txt
for r in 1000 1500 2000 2500 3000 3500 4000
do
	./fractalGenCU 8 $r $r 5 0 >> timingOut.txt
done

echo "OpenMP 8 Threads"
echo "OpenMP 8 Threads" >> timingOut.txt
./fractalGenMP 8 500 500 1 >> timingOut.txt
for r in 1000 1500 2000 2500 3000 3500 4000
do
	./fractalGenMP 8 $r $r 0 >> timingOut.txt
done

date >> timingOut.txt

rm testRotate.avi
python plotTiming.py

#git add *
#git commit -m "finished tests"
#git push