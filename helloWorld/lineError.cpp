#include <stdio.h>

void errorFunc();

int main()
{
	printf("hello from line %d in file %s\n", __LINE__, __FILE__);
	errorFunc();

	return 0;
}

void errorFunc()
{
	printf("Hello from %s on line %d at %s on %s\n", __func__, __LINE__, __TIME__, __DATE__);
}