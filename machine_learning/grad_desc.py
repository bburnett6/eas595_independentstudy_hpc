import numpy as np 
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt 

def f(xs, A, b):
	return 0.5 * np.linalg.norm(np.matmul(A, xs) - b, ord=2)**2


def grad_desc(A, b, stepsize=0.01, tolerance=0.00001):
	transA = np.transpose(A)
	xs = np.random.rand(A.shape[0], 1)
	all_x = []
	all_y = []

	while (np.linalg.norm(np.matmul(np.matmul(transA, A), xs) - np.matmul(transA, b), ord=2) > tolerance):
		xs = xs - stepsize * np.matmul(np.matmul(transA, A), xs) - np.matmul(transA, b)
		all_x.append(xs[0])
		all_y.append(xs[1])

	return xs, np.array(all_x), np.array(all_y)

#a = np.random.rand(2, 2)
a = np.array([[.5, 1], [.5, 1]])
b = np.array([[0], [0]])

m, xr, yr = grad_desc(a, b)

x = np.linspace(-1, 1, num=15)
y = np.linspace(-1, 1, num=15)
X, Y = np.meshgrid(x, y)
Z = np.zeros(X.shape)
for j, _ in enumerate(y):
	for i, _ in enumerate(x):
		Z[j][i] =  f([ X[i][j], Y[i][j]], a, b)

zr = []
for i, _ in enumerate(xr):
	zr.append(f([xr[i], yr[i]], a, b))
zr = np.array(zr)

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_surface(X, Y, Z, cmap='viridis')

ax.scatter3D(xr, yr, zr, c=zr, cmap='hot')

plt.show()
plt.savefig("out.pdf")