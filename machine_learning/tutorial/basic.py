from __future__ import absolute_import, division, print_function

import tensorflow as tf 
from tensorflow import keras

import numpy as np 
import matplotlib.pyplot as plt 

print(tf.__version__)

fashion_mnist = keras.datasets.fashion_mnist

(train_imgs, train_labels), (test_imgs, test_labels) = fashion_mnist.load_data()

class_names = ['top', 'pants', 'pullover', 'dress', 'coat',
				'sandal', 'shirt', 'sneaker', 'bag', 'boot']

train_imgs = train_imgs / 255.0
test_imgs = test_imgs / 255.0

'''
plt.figure(figsize=(10,10))
for i in range(25):
	plt.subplot(5, 5, i + 1)
	plt.xticks([])
	plt.yticks([])
	plt.grid(False)
	plt.imshow(train_imgs[i], cmap=plt.cm.binary)
	plt.xlabel(class_names[train_labels[i]])
plt.show()
'''

model = keras.Sequential([
	keras.layers.Flatten(input_shape=(28,28)),
	keras.layers.Dense(128, activation=tf.nn.relu),
	keras.layers.Dense(10, activation=tf.nn.softmax)
	])

model.compile(optimizer='adam',
	loss='sparse_categorical_crossentropy',
	metrics=['accuracy'])

model.fit(train_imgs, train_labels, epochs=5)

test_loss, test_acc = model.evaluate(test_imgs, test_labels)

print("Test Accuracy:", test_acc)
