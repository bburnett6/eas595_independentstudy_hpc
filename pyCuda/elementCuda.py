#!/bin/python
import pycuda.gpuarray as gpuarray
import pycuda.autoinit

from pycuda.elementwise import ElementwiseKernel

# define elementwise `add()` function
add = ElementwiseKernel(
        "float *a, float *b, float *c",
        "c[i] = a[i] + b[i]",
        "add")

# create a couple of random matrices with a given shape
from pycuda.curandom import rand as curand
import argparse

parser = argparse.ArgumentParser(description='Add two randomly initialized matrices of size n by m')
parser.add_argument('n', type=int, help='the n dimension of the matrices')
parser.add_argument('m', type=int, help='the m dimension of the matrices')
args = parser.parse_args()

n = args.n
m = args.m
shape = n, m
a_gpu = curand(shape)
b_gpu = curand(shape)

# compute sum on a gpu
c_gpu = gpuarray.empty_like(a_gpu)
add(a_gpu, b_gpu, c_gpu)

# check the result
#import numpy.linalg as la
#print (c_gpu - (a_gpu + b_gpu))
#assert la.norm((c_gpu - (a_gpu + b_gpu)).get()) < 1e-5