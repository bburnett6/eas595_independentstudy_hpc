#include <stdio.h>
#include <stdlib.h>

float** alloc_2d_float(int rows, int cols) {
    float* data = (float*)malloc(rows * cols * sizeof(float));
    float** array = (float**)malloc(rows * sizeof(float));
    int i;
    for (i = 0; i < rows; i++) {
            array[i] = &(data[cols*i]);
    }

    return(array);
}

float randFl()
{
	return (float)rand() / (float)RAND_MAX;	
}

__global__ void add_mat(float *a, float *b, float *c, int n)
{
	int i = threadIdx.x;
	int j = threadIdx.y;

	if (i < n || j < n)
		c[n * j + i] = a[n * j + i] + b[n * j + i];
}

__global__ void add_mat2(float **a, float **b, float **c, int n)
{
	int i = threadIdx.x;
	int y = threadIdx.y;

	if (i < n || j < n)
		c[i][j] = a[i][j] + b[i][j];
}

int main(int argc, char** argv)
{
	if (argc <= 1 || argc > 2)
	{
		printf("Please provide n where n is the dimension of the 2d n by n matrix\n");
		exit(1);
	}
	int n = atoi(argv[1]);
	srand(time(NULL));
	float a[n*n], b[n*n], c[n*n];
	float *gpu_a, *gpu_b, *gpu_c;

	cudaMalloc((void**)&gpu_a, n * n * sizeof(float));
	cudaMalloc((void**)&gpu_b, n * n * sizeof(float));
	cudaMalloc((void**)&gpu_c, n * n * sizeof(float));

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			a[n * j + i] = randFl();
			b[n * j + i] = randFl();
			c[n * j + i] = 0;
		}
	}

	cudaMemcpy(gpu_a, a, n * n * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(gpu_b, b, n * n * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(gpu_c, c, n * n * sizeof(float), cudaMemcpyHostToDevice);

	int nBlk = 1;
	dim3 threadsPerBlk(n, n);

	add_mat2<<<nBlk, threadsPerBlk>>>(gpu_a, gpu_b, gpu_c, n);

	cudaMemcpy(c, gpu_c, n * n * sizeof(float), cudaMemcpyDeviceToHost);

/*
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			printf("%f + %f = %f\t", a[n * j + i], b[n * j + i], c[n * j + i]);
		printf("\n");
	}
*/

	cudaFree(gpu_a);
	cudaFree(gpu_b);
	cudaFree(gpu_c);

	return 0;
}

