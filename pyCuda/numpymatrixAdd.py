#!/bin/python
import numpy as np 
import argparse

parser = argparse.ArgumentParser(description='Add two randomly initialized matrices of size n by m')
parser.add_argument('n', type=int, help='the n dimension of the matrices')
parser.add_argument('m', type=int, help='the m dimension of the matrices')
args = parser.parse_args()

n = args.n
m = args.m
a = np.random.randn(n, m)
b = np.random.randn(n, m)
a = a.astype(np.float32)
b = b.astype(np.float32)

c = a + b 
