#!/bin/python
import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule
import numpy as np 
import argparse

parser = argparse.ArgumentParser(description='Add two randomly initialized matrices of size n by n')
parser.add_argument('n', type=int, help='the dimension of the matrices')
args = parser.parse_args()

n = args.n
a = np.random.randn(n, n)
b = np.random.randn(n, n)
a = a.astype(np.float32)
b = b.astype(np.float32)
c = np.empty_like(a)
a_dev = cuda.mem_alloc(a.nbytes)
b_dev = cuda.mem_alloc(b.nbytes)
c_dev = cuda.mem_alloc(c.nbytes)
cuda.memcpy_htod(a_dev, a)
cuda.memcpy_htod(b_dev, b)
cuda.memcpy_htod(c_dev, c)

mod = SourceModule("""
	__global__ void add(float *a, float *b, float *c, int n)
	{
		int idx = threadIdx.x + blockDim.x * blockIdx.x;
		int idy = threadIdx.y + blockDim.y * blockIdx.y;

		if (idx < n || idy < n)
			c[idx] = a[idx] + b[idx]; //not done...
	}
	""")

func = mod.get_function("add")
func(a_dev, b_dev, c_dev, cuda.In(n), block=(n, n, 1))

c_ret = np.empty_like(c)
cuda.memcpy_dtoh(c_ret, c_dev)
#print(a)
#print(b)
#print(a_ret)