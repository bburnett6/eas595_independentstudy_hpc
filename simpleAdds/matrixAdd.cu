#include <stdio.h>

__global__ void add_mat(int *a, int *b, int *c, int n)
{
	int i = threadIdx.x;
	int j = threadIdx.y;

	if (i < n || j < n)
		c[n * j + i] = a[n * j + i] + b[n * j + i];
}

int main()
{
	int n = 3;
	int a[n*n], b[n*n], c[n*n];
	int *gpu_a, *gpu_b, *gpu_c;

	cudaMalloc((void**)&gpu_a, n * n * sizeof(int));
	cudaMalloc((void**)&gpu_b, n * n * sizeof(int));
	cudaMalloc((void**)&gpu_c, n * n * sizeof(int));

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			a[n * j + i] = i + i * j;
			b[n * j + i] = j + i * j;
			c[n * j + i] = 0;
		}
	}

	cudaMemcpy(gpu_a, a, n * n * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(gpu_b, b, n * n * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(gpu_c, c, n * n * sizeof(int), cudaMemcpyHostToDevice);

	int nBlk = 1;
	dim3 threadsPerBlk(n, n);

	add_mat<<<nBlk, threadsPerBlk>>>(gpu_a, gpu_b, gpu_c, n);

	cudaMemcpy(c, gpu_c, n * n * sizeof(int), cudaMemcpyDeviceToHost);

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			printf("%d + %d = %d\t", a[n * j + i], b[n * j + i], c[n * j + i]);
		printf("\n");
	}

	cudaFree(gpu_a);
	cudaFree(gpu_b);
	cudaFree(gpu_c);

	return 0;
}