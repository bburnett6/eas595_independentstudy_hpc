#include <stdio.h>

#define n 20

__global__ void add_vec(int *a, int *b, int *c)
{
	int index = blockIdx.x;
	if (index < n)
		c[index] = a[index] + b[index];
}

int main()
{
	int a[n], b[n], c[n];
	int *gpu_a, *gpu_b, *gpu_c;

	cudaMalloc((void**)&gpu_a, n * sizeof(int));
	cudaMalloc((void**)&gpu_b, n * sizeof(int));
	cudaMalloc((void**)&gpu_c, n * sizeof(int));

	for (int i = 0; i < n; i++)
	{
		a[i] = i;
		b[i] = i * i;
	}

	cudaMemcpy(gpu_a, a, n * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(gpu_b, b, n * sizeof(int), cudaMemcpyHostToDevice);

	add_vec<<<n, 1>>>(gpu_a, gpu_b, gpu_c);

	cudaMemcpy(c, gpu_c, n * sizeof(int), cudaMemcpyDeviceToHost);

	for (int i = 0; i < n; i++)
		printf("%d + %d = %d\n", a[i], b[i], c[i]);

	cudaFree(gpu_a);
	cudaFree(gpu_b);
	cudaFree(gpu_c);

	return 0;
}